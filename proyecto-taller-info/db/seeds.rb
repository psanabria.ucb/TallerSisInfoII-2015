# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

AopType.create([{name: 'Industrial', description: 'Iipo de AOP especifico para construcciones dedicadas a produccion industria.'}, {name:'Comercial', description: 'Tipo de AOP especifico para entidades comerciales.'}])

Role.create([{typename: 'Admin'}, {typename: 'Receptionist'}, {typename: 'Legal Agent'}, {typename: 'Unity Chief'}, {typename: 'Revisor'}])

test_user = User.new
test_user.email = 'admin@gmail.com'
test_user.username = 'admin'
test_user.active = true
test_user.password = 'adminadmin'
test_user.role = 1
test_user.save!

test_user = User.new
test_user.email = 'recepcionist@gmail.com'
test_user.username = 'recepcionist'
test_user.active = true
test_user.password = 'adminadmin'
test_user.role = 2
test_user.save!

test_user = User.new
test_user.email = 'legalAgent1@gmail.com'
test_user.username = 'legalAgent1'
test_user.active = true
test_user.password = 'adminadmin'
test_user.role = 3
test_user.save!

test_user = User.new
test_user.email = 'jefeUnidad@gmail.com'
test_user.username = 'jefeUnidad'
test_user.active = true
test_user.password = 'adminadmin'
test_user.role = 4
test_user.save!

test_user = User.new
test_user.email = 'revisor@gmail.com'
test_user.username = 'revisor'
test_user.active = true
test_user.password = 'adminadmin'
test_user.role = 5
test_user.save!