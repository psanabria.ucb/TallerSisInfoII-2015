class LegalAgentsController < ApplicationController
  before_action :set_legal_agent, only: [:show, :edit, :update, :destroy]

  # GET /legal_agents
  # GET /legal_agents.json
  def index
    @legal_agents = LegalAgent.all

    if params[:search]
      @legal_agents = LegalAgent.search(params[:search]).order('created_at DESC')
    else
      @legal_agents = LegalAgent.all.order('created_at ASC')
    end

  end

  # GET /legal_agents/1
  # GET /legal_agents/1.json
  def show
  end

  # GET /legal_agents/new
  def new
    @legal_agent = LegalAgent.new
    @users = User.all
    @companies = Company.all
  end

  # GET /legal_agents/1/edit
  def edit
    @users = User.all
    @companies = Company.all
  end

  # POST /legal_agents
  # POST /legal_agents.json
  def create
    @legal_agent = LegalAgent.new(legal_agent_params)
    @users = User.all
    @companies = Company.all
    respond_to do |format|
      if @legal_agent.save
        format.html { redirect_to @legal_agent, notice: 'Representante Legal creado exitosamente' }
        format.json { render :show, status: :created, location: @legal_agent }
      else
        format.html { render :new }
        format.json { render json: @legal_agent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /legal_agents/1
  # PATCH/PUT /legal_agents/1.json
  def update
    @users = User.all
    @companies = Company.all
    respond_to do |format|
      if @legal_agent.update(legal_agent_params)
        format.html { redirect_to @legal_agent, notice: 'Representante Legal actualizado exitosamente' }
        format.json { render :show, status: :ok, location: @legal_agent }
      else
        format.html { render :edit }
        format.json { render json: @legal_agent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /legal_agents/1
  # DELETE /legal_agents/1.json
  def destroy
    @legal_agent.destroy
    respond_to do |format|
      format.html { redirect_to legal_agents_url, notice: 'Representante Legal eliminado' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_legal_agent
      @legal_agent = LegalAgent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def legal_agent_params
      params.require(:legal_agent).permit(:identification_number, :identification_type, :first_name, :last_name, :country, :city, :address, :phone_number, :email, :mailbox, :user_id, :company_id)
    end
end
