class StaticPagesController < ApplicationController

  def home
  end

  def error
    render 'static_pages/Error'
  end

end
