json.extract! @person, :id, :identification_number, :identification_type, :firs_name, :last_name, :country, :city, :address, :email, :profession, :position, :user_id, :created_at, :updated_at
