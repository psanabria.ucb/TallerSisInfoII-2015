json.array!(@people) do |person|
  json.extract! person, :id, :identification_number, :identification_type, :firs_name, :last_name, :country, :city, :address, :email, :profession, :position, :user_id
  json.url person_url(person, format: :json)
end
