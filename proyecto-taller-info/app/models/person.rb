class Person < ActiveRecord::Base
  belongs_to :user

  validates :identification_number, presence: {:message => ' no debe dejarse en blanco.'}
  validates :identification_number, uniqueness: {:message => '  no debe repetirse.'}
  validates :identification_number, numericality: { only_integer: true, message: ' debe ser un numero' }
  # validates :identification_number, :presence => true, :uniqueness => {:scope => :identification_type}
  # validates :identification_type, presence: {:message => ' (Tipo de identificacion) no debe dejarse en blanco.'}
  # validates :identification_number, uniqueness: { scope: [:identification_type] }
  validates :firs_name, presence: {:message => '  no debe dejarse en blanco.'}
  validates :last_name, presence: {:message => '  no debe dejarse en blanco.'}
  validates :country, presence: {:message => '  no debe dejarse en blanco.'}
  validates :city, presence: {:message => '  no debe dejarse en blanco.'}
  validates :address, presence: {:message => '   no debe dejarse en blanco.'}
  validates :email, presence: {:message => ' no debe dejarse en blanco.'}
  validates :position, presence: {:message => '  no debe dejarse en blanco.'}
  validates :profession, presence: {:message => ' no debe dejarse en blanco.'}
  validates :email, uniqueness: {:message =>' ya existe'}
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create, message: 'es invalido'}

  def self.search(search)
    where('firs_name LIKE :search OR last_name LIKE :search', search:"%#{search}%")
  end

end
