class Company < ActiveRecord::Base
	has_many :legal_agents, :dependent => :delete_all

	validates :name, :presence => {:message => ' no debe dejarse en blanco.'}
	validates :name, uniqueness: {message: ' ya existe, no se debe duplicar'}
	validates :principal_activity, :presence => {:message => ' no debe dejarse en blanco.'}
	validates :nit, :presence => {:message => ' no debe dejarse en blanco.'}
	validates :nit, uniqueness: {message: ' ya existe, no se debe duplicar'}
	validates :nit, numericality: { only_integer: true, message: ' debe ser un numero' }
	validates :address, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :canton, :presence => {:message => '  no debe dejarse en blanco.'} 
	validates :province, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :departament, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :phones, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :phones, numericality: { only_integer: true, message: ' debe ser un numero' }
	#validates :fax, :presence => {:message => '  (Fax) no debe dejarse en blanco.'} 
	validates :mailbox, :presence => {:message => ' no debe dejarse en blanco.'}

	def self.search(search)
		where("name LIKE :search ", search:"%#{search}%")
	end

end
