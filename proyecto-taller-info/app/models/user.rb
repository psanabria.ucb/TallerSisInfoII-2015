class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessor :login

  # validates :email, :presence => {:message => 'es requerido'}
  # validates :email, format:{ :multiline => true, with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/, message: 'no tiene formato invalido'}
  validates :username, :presence => {:message => ' es necesario.'}
  validates :username, :uniqueness => {:message => ' ya esta siendo utilizado.'}
  validates :username, length: {minimum: 5, :message => 'no debe tener menos de 5 caracteres.'}
  validates :username, length: {maximum: 25, :message => 'no debe tener mas de 25 caracteres.'}
  # validates :password, :presence => {:message => ' es necesario'}

  def self.find_for_database_authentication(conditions={})
    find_by(username: conditions[:login])
  end

  def self.search(search)
    where("username LIKE ?", "%#{search}%")
  end
end
