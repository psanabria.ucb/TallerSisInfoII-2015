class LegalAgent < ActiveRecord::Base
  	
  	belongs_to :user
  	belongs_to :company

	validates :identification_number, presence: {:message => ' no debe dejarse en blanco'}
	validates :identification_number, numericality: { only_integer: true, message: ' debe ser un numero' }
	validates :identification_number, uniqueness: { only_integer: true, message: ' no debe repetirse' }
	# validates :identification_type, presence: {:message => ' no debe dejarse en blanco'}
	validates :first_name, presence: {:message => ' no debe dejarse en blanco'}
	validates :last_name, presence: {:message => ' no debe dejarse en blanco'}
	validates :country, presence: {:message => ' no debe dejarse en blanco'}
	validates :city, presence: {:message => ' no debe dejarse en blanco'}
	validates :address, presence: {:message => ' no debe dejarse en blanco'}
	validates :phone_number, presence: {:message => ' no debe dejarse en blanco'}
	validates :phone_number, numericality: { only_integer: true, message: ' debe ser un numero' }
#	validates :mailbox, presence: {:message => ' (Codigo Postal) no debe dejarse en blanco.'}

	def self.search(search)
		where('first_name LIKE :search OR last_name LIKE :search', search:"%#{search}%")
	end

end
