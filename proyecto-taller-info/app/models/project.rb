class Project < ActiveRecord::Base
  belongs_to :aop_type
  belongs_to :companty


  validates :name, :presence => {:message => ' no debe dejarse en blanco.'}
	validates :name, uniqueness: {message: ' ya existe, no se debe duplicar'}
	validates :description, :presence => {:message => ' no debe dejarse en blanco.'}	
	validates :address, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :canton, :presence => {:message => ' no debe dejarse en blanco.'}
	validates :province, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :departament, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :latitude, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :latitude, numericality: { only_integer: true, message: ' debe ser un numero' }
    validates :longitude, :presence => {:message => ' no debe dejarse en blanco.'} 
	validates :longitude, numericality: { only_integer: true, message: ' debe ser un numero' }
	#validates :fax, :presence => {:message => '  (Fax) no debe dejarse en blanco.'} 

  def self.search(search)
 
   where("name LIKE :search ", search:"%#{search}%") 
   
end

end
