// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require scripts.js
//= require bootstrap-sprockets
//= require realtime_validations
//= require_tree .


$(document).ready(function(){

    $('#loginButton').on('click', function(event) {
        var login = $('#login')[0].value;
        var password = $('#password')[0].value;
        if (login.length == 0 && password.length == 0) {
            event.preventDefault();
            alert('Debe introducir su contrasena y su nombre de ususario.');
        }
        else if (login.length == 0){
            event.preventDefault();
            alert('Debe introducir su nombre de ususario.');
        }
        else if(password.length == 0){
            event.preventDefault();
            alert('Debe introducir su contrasena.');
        }
    });
});